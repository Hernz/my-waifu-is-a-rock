
extends KinematicBody2D

signal player_has_rock(b)

onready var player = get_node("/root/Level/Player")
onready var level = get_node("/root/Level")

var gravity : float = -500
export var rebond : float = 0.3
export var speed : float = 200

export var pickup_deny_delay : float = 0.3

var pickup_deny_timer : float = 0

var velocity = Vector2()

var shooted = false

var d_gravity : float = 0
var d_velocity = Vector2()
var d_dt : float = 0
var d_previsualiser = false
var nb_points = 100
var points = PoolVector2Array()

func _ready():
	#shoot(Vector2.ONE,100.0,100.0)
	self.set_physics_process(true)
	connect("player_has_rock",player,"set_has_rock")
	
	
func previsualiser(direction : Vector2 ,distance : float,height : float, dt : float):
	d_gravity = -(2*height)/pow((distance/2)/(2*speed),2)
	d_velocity = Vector2()
	d_velocity.x = distance/(sqrt(-2*height/d_gravity))
	d_velocity.y = -sqrt(-2*d_gravity*height)
	d_velocity.y *= -1
	d_velocity *= direction
	d_dt = dt
	d_previsualiser = true
	
func masquer_previsualisation():
	d_previsualiser = false
	
func _draw():
	"""if Engine.is_editor_hint():
		draw_circle(Vector2(0,0),20,Color(1,1,1))"""
	if d_previsualiser:
		"""
		for p in points:
			draw_circle(p, 5, Color(1,1,1))
		"""
		$Line2D.points = points
	else :
		$Line2D.clear_points()

func build_previsualisation():
	if d_previsualiser:
		points = PoolVector2Array()
		var lastPos = position
		for i in range(nb_points + 1):
			var t = (i*d_dt)
			var pos = Vector2()
			pos.x = d_velocity.x*t
			pos.y = position.y-0.5*d_gravity*t*t+d_velocity.y*t
			var space_state = get_world_2d().direct_space_state
			var result = space_state.intersect_ray(self.to_global(lastPos), self.to_global(pos),[self],collision_mask)     
			if(result):
				return
			points.push_back(pos)
			lastPos = pos

func shoot(direction : Vector2 ,distance : float,height : float):
	shooted = true
	position = global_position
	$PickUpBox/CollisionShape2D.set_deferred("disabled",true)
	$ActivationBox/CollisionShape2D.set_deferred("disabled",false)
	pickup_deny_timer = 0
	if self.get_parent() != null:
		self.get_parent().remove_child(self)
	level.call_deferred("add_child",self)
	$RockHitBox/CollisionShape2D.disabled = false
	emit_signal("player_has_rock",false)
	
	height = clamp(height,1,1000000)
	"""velocity.x = distance/(sqrt(-2*height/gravity))
	velocity.y = -sqrt(-2*gravity*height)"""
	"""velocity.x = distance/(sqrt(-2*height/gravity))
	velocity.y = -sqrt(speed*speed-2*gravity*height)"""
	shoot2(distance,height,(distance/2)/(2*speed))
	velocity.y *= -1
	velocity *= direction
	
#/!\ change la gravité
func shoot2(distance,height,timeToReachApex):
	gravity = -(2*height)/pow(timeToReachApex,2)
	velocity.x = distance/(sqrt(-2*height/gravity))
	velocity.y = -sqrt(-2*gravity*height)
	#print(velocity)
	#print(gravity)

func on_hit(pos):
	velocity = Vector2.ZERO
	#position = pos

func _physics_process(delta):
	if(shooted):
		if pickup_deny_timer < pickup_deny_delay:
			pickup_deny_timer += delta
		if pickup_deny_timer >= pickup_deny_delay:
			$PickUpBox/CollisionShape2D.disabled = false
		
		velocity.y -= gravity*delta
		var collision = move_and_collide(velocity*delta)
		if(collision):
			velocity = velocity.bounce(collision.normal)*rebond
			if (abs(velocity.x) > 10 or abs(velocity.y) > 10) and abs(gravity) <= 15000 :
				$Sounds/HitWall.play()
	else:
		build_previsualisation()