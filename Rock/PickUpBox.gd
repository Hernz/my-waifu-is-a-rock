extends Area2D

signal player_has_rock()

onready var player = get_node("/root/Level/Player")
onready var rock = get_node("..")

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("area_entered",self,"_on_area_entered")
	connect("player_has_rock",player,"set_has_rock")

func _on_area_entered(area):
	if area == player.get_node("PickUpBox"):
		call_deferred("reparent")
	
func reparent():
	#Si c'est cassez ca vient p-e d'ici
	#rock.set_physics_process(false)
	rock.shooted = false
	rock.velocity = Vector2.ZERO
	#rock.position = Vector2(40,0)
	rock.get_parent().remove_child(rock)
	player.add_child(rock)
	get_node("../RockHitBox/CollisionShape2D").disabled = true
	get_node("../ActivationBox/CollisionShape2D").disabled = true
	emit_signal("player_has_rock",true)