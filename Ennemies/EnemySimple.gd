extends KinematicBody2D

signal ennemi_died()
signal camera_shake(duration)

export var starting_direction : Vector2 = Vector2.LEFT
export var speed : int = 100
export var gravity : int = 200

onready var camera = get_node("/root/Level/Camera2D")

var direction : Vector2 = starting_direction
var velocity : Vector2 = Vector2.ZERO

var dead : bool = false
var to_delete : bool = false

func _ready():
	connect("camera_shake",camera,"_camera_shake") 

func connection():
	$EnemyHitBox.connections()

func _physics_process(delta):
	velocity = direction*speed
	if is_on_floor():
		velocity.y = 0
	else: 
		velocity.y += gravity
	
	velocity = move_and_slide(velocity,Vector2.UP)
	if velocity == Vector2.ZERO:
		direction *= -1

func die():
	dead = true
	$Sounds/Die.play()
	$CollisionShape2D.set_deferred("disabled",true)
	$EnemyHitBox/CollisionShape2D.set_deferred("disabled",true)
	set_physics_process(false)
	emit_signal("camera_shake",0.2)
	$AnimationPlayer.play("Die")
	emit_signal("ennemi_died")
	if to_delete:
		yield($AnimationPlayer,"animation_finished")
		queue_free()
	
func reset():
	$AnimationPlayer.play_backwards("Die")
	yield($AnimationPlayer,"animation_finished")
	$CollisionShape2D.set_deferred("disabled",false)
	$EnemyHitBox/CollisionShape2D.set_deferred("disabled",false)
	set_physics_process(true)
	dead = false
	to_delete = false
	
func remove():
	if dead :
		queue_free()
	else :
		to_delete = true