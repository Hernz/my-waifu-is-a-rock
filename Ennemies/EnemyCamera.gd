extends KinematicBody2D

signal camera_shake(duration)

export var speed : int = 1
export var gravity : int = 200
export var angle : float = 30

onready var camera = get_node("/root/Level/Camera2D")

var isHurt = false
var direction = 1

func _ready():
	connect("camera_shake",camera,"_camera_shake") 
	rotation_degrees = angle

func connection():
	$EnemyHitBox.connections()

func _physics_process(delta):
	if(!isHurt):
		rotation_degrees += delta*speed*direction
	if(abs(rotation_degrees) > angle):
		direction *= -1


func die():
	#$CollisionShape2D.set_deferred("disabled",true)
	#$EnemyHitBox/CollisionShape2D.set_deferred("disabled",true)
	#set_physics_process(false)
	if(isHurt):
		 return
	emit_signal("camera_shake",0.2)
	$AnimationPlayer.play("Die")
	isHurt = true
	$Detection/cone.call_deferred("set_disabled", true)
	yield(get_tree().create_timer(3),"timeout")
	$Detection/cone.call_deferred("set_disabled", false)
	isHurt = false
	$AnimationPlayer.play("Idle")
	#queue_free()