extends Area2D

onready var player_hitbox = get_node("/root/Level/Player/PlayerHitBox")
onready var level = get_node("/root/Level")

var rock

func _ready():
	rock = level.find_node("Rock")
	connect("area_entered",player_hitbox,"_on_enemie_hit")

func connections():
	level.find_node("Rock").get_node("RockHitBox").connect("area_entered",self,"_on_rock_hit")
	#get_parent().get_parent().rock.get_node("RockHitBox").connect("area_entered",self,"_on_rock_hit")
	
func _on_rock_hit(area):
	if not rock.is_on_floor() and area == self:
		get_parent().die()