extends EmptyLevel

onready var camera = find_node("Camera2D")
onready var area = $Event
onready var rock = $Rock.get_node("PickUpBox")

var deja_lancee = false

func _ready():
	area.connect("area_entered", self, "spawn_rock")
	rock.connect("player_has_rock", self, "rock_taken")
	$BoutonActivateur.connect_to($SimpleDoor, "open")

func spawn_rock(area):
	if not deja_lancee:
		#$AudioStreamPlayer2D.play()
		deja_lancee = true
		$lumiere_jesus.enabled = true
		$Rock.position = $new_rock.position
		$Rock.shoot(Vector2(0, 10), 150, 0)
		yield(get_tree().create_timer(1),"timeout")
		camera.change_target($Rock)
		#$Camera2D.target = $Rock
	
func rock_taken(b):
	if b:
		stop_spawn()
		rock.disconnect("player_has_rock", self, "rock_taken")
		
func stop_spawn():
	$lumiere_jesus.enabled = false
	$AudioStreamPlayer2D.stop()
	camera.change_target($Player)
	#$Camera2D.target = $Player