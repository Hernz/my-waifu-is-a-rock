shader_type canvas_item;

uniform float uvY = 1;
uniform float speed = 1;

void fragment(){
	COLOR = texture(TEXTURE, vec2(1,uvY)*UV+vec2(0,-TIME*speed));
}