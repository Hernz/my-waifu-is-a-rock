extends StaticBody2D

export var closed : bool = true
export var largeur : float = 2

func _ready():
	$CollisionShape2D.scale.y = largeur
	$CollisionShape2D.position.y = $CollisionShape2D.shape.extents.y*largeur+$CollisionShape2D.shape.extents.y
	$barre_bleu.scale.y *= largeur
	$barre_bleu.position.y = $CollisionShape2D.shape.extents.y*largeur+$CollisionShape2D.shape.extents.y
	if closed:
		close()
	else:
		open()

func open():
	$porte_lumiere/lumiere.modulate = Color("3cef2d")
	set_collision_layer_bit(0,false)
	$barre_bleu.hide()

func close():
	$porte_lumiere/lumiere.modulate = Color("ef2d2d")
	set_collision_layer_bit(0,true)
	$barre_bleu.show()
	
func reset():
	if closed:
		close()
	else:
		open()