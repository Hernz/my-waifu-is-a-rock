extends Area2D

signal checkpoint_reset()
signal checkpoint_delete()

func _ready():
	$CollisionShape2D.disabled = true
	var detection_rect : Rect2 = Rect2(global_position,$CollisionShape2D.shape.extents*scale*2)
	for node in get_tree().get_nodes_in_group("Resetable"):
		if detection_rect.has_point(node.global_position):
			connect("checkpoint_reset",node,"reset")
			if node.is_in_group("Deletable"):
				connect("checkpoint_delete",node,"remove")

func reset():
	emit_signal("checkpoint_reset")
	
func delete_trash():
	emit_signal("checkpoint_delete")