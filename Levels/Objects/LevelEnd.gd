extends Area2D

signal level_change(path)

onready var player_box = get_node("/root/Level/Player/PlayerHitBox")
onready var level = get_node("/root/Level")

export var next_level_path : String = ""

func _ready():
	connect("area_entered",self,"_on_area_entered")
	connect("level_change",level,"_on_level_change")
	
func _on_area_entered(area):
	if area == player_box:
		emit_signal("level_change",next_level_path)