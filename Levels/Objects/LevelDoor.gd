extends StaticBody2D

onready var player_area = get_node("/root/Level/Player/PlayerHitBox")
onready var player = get_node("/root/Level/Player")

export var closed : bool = true
export var largeur : float = 2

func _ready():
	$Area2D.connect("area_entered",player_area,"_on_entering_levelDoor")
	$Area2D.connect("area_exited",player_area,"_on_exiting_levelDoor")
	if closed:
		close()
	else:
		open()
	$CollisionShape2D.scale.y = largeur
	$CollisionShape2D.position.y = $CollisionShape2D.shape.extents.y*largeur-$CollisionShape2D.shape.extents.y
	$Area2D.scale.y = largeur
	$barre_bleu.scale.y *= largeur
	$aura_bleu.scale.y *= largeur
	$aura_bleu.get_material().set_shader_param("uvY",largeur*2)

func _process(delta):
	if not closed :
		if player.has_rock:
			$porte_lumiere/lumiere.modulate = Color("3cef2d")
		else :
			$porte_lumiere/lumiere.modulate = Color("fdff30")

func open():
	$porte_lumiere/lumiere.modulate = Color("3cef2d")
	$barre_bleu.visible = false
	$aura_bleu.visible = true
	closed = false
	set_collision_layer_bit(0,false)
	
func close():
	$porte_lumiere/lumiere.modulate = Color("ef2d2d")
	$barre_bleu.visible = true
	$aura_bleu.visible = false
	closed = true
	set_collision_layer_bit(0,true)
	
func reset():
	if closed:
		close()
	else:
		open()