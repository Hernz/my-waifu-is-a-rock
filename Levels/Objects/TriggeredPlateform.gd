extends StaticBody2D

onready var level = get_node("/root/Level")

export var withdraw_delay : float = 4
export var deploy_delay : float = 0.4
export var rock_throw_distance : float = 75

var withdraw_timer : float = 0
var deploy_timer : float= 0
var withdraw : bool = false
var rock

var init : bool = false 

func _ready():
	rock = level.find_node("Rock")
	withdraw()
	$TriggerBox/CollisionShape2D.set_deferred("disabled",false)
	set_process(false)
	withdraw = false
	init = true

func deploy():
	$TriggerBox/CollisionShape2D.set_deferred("disabled",true)
	$BoundingBox.set_deferred("disabled",false)
	$BoundingBox.show()
	$TriggerBox.hide()
	$Sounds/Activation.play()
	$AnimationPlayer.play("Deploy")
	withdraw_timer = 0
	rock.position = $RockDeployPos.global_position
	rock.call_deferred("shoot",Vector2.UP,rock_throw_distance,rock_throw_distance)
	set_process(true)

func withdraw():
	$BoundingBox.set_deferred("disabled",true)
	if init:
		$Sounds/Activation.play()
	$AnimationPlayer.play_backwards("Withdraw")
	withdraw = true
	yield($AnimationPlayer,"animation_finished")
	$BoundingBox.hide()
	$TriggerBox.show()
	
func _process(delta):
	if not withdraw:
		withdraw_timer += delta
		if withdraw_timer >= withdraw_delay:
			withdraw()
	else: 
		deploy_timer += delta
		if deploy_timer >= deploy_delay:
			deploy_timer = 0
			$TriggerBox/CollisionShape2D.set_deferred("disabled",false)
			withdraw = false
			set_process(false)
			
func reset():
	withdraw()