extends Area2D

signal checkpoint_changed(position,reset_area)

onready var player_hitbox = get_node("/root/Level/Player/PlayerHitBox")
onready var level = get_node("/root/Level")

var active : bool = true

func _ready():
	connect("area_entered",self,"_checkpoint_entered")
	connect("checkpoint_changed",level,"_on_checkpoint_changed")
	
func _checkpoint_entered(area):
	if area == player_hitbox:
		active = false
		$CollisionShape2D.call_deferred("disabled",true)
		emit_signal("checkpoint_changed",get_parent().position,get_node("../ResetArea"))