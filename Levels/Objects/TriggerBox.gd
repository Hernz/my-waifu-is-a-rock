extends Area2D

onready var level = get_node("/root/Level")

func _ready():
	level.find_node("Rock").get_node("ActivationBox").connect("area_entered",self,"_on_rock_hit")

func _on_rock_hit(area):
	if area == self:
		get_parent().deploy()