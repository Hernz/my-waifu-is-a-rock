extends Area2D

signal pressed()

onready var level = get_node("/root/Level")

export var connected_nodes : Array = []
export var callback_functions : Array = []

var locked : bool = false

func _ready():
	$CollisionShape2D/btn_poussoir.self_modulate = Color("dd1b1b")
	connect("area_entered",self,"_on_area_entered")
	if connected_nodes != [] and connected_nodes.size()==callback_functions.size():
		for i in range(connected_nodes.size()):
			connect_to(level.find_node(connected_nodes[i]),callback_functions[i])

func connect_to(node, callback_func : String):
	connect("pressed",node,callback_func)

func _on_area_entered(area):
	if not locked:
		locked = true
		$Sounds/Activate.play()
		$AnimationPlayer.play("open")
		emit_signal("pressed")
	
func reset():
	$AnimationPlayer.play_backwards("open")
	$CollisionShape2D/btn_poussoir.self_modulate = Color("dd1b1b")
	yield(get_tree().create_timer(1),"timeout")
	locked = false