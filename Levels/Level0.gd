extends EmptyLevel

func _ready():
	$BoutonActivateur2.connect_to($LevelDoor2,"open")
	$BoutonActivateur3.connect_to($LevelDoor,"open")
	$BoutonActivateur4.connect_to($LevelDoor6,"open")
	connect("reseted", get_node("Mobs waves Event"), "reset_event")
	$Player.connect("player_dies", get_node("Mobs waves Event"), "reset_event")