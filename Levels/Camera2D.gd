extends Camera2D

onready var player = get_node("/root/Level").find_node("Player")

export var target = 0
export var offset_speed = 2.5
export var speed = 5
export var mouse_dead_zone : Vector2 = Vector2(1,1)
export var idle_offset : Vector2 = Vector2(0,-100)
export var mouse_offset = 150

#Shake configuration
export var shake_amplitude : Vector2 = Vector2(1,1)
export(float, EASE) var SHAKE_DAMP_EASING = 1.0

# Curser following configuration
export var follow_offset = Vector2(0.1,0.1)
export var follow_speed = 200

# Shake vars
var shake : bool = false
var shake_timer : float = 0
var shake_duration : float = 0.8 # sec

#var old_position : Vector2 = Vector2()

func _ready():
	randomize()
	target = player
	current = true
	zoom = Vector2(1.1,1.1)

func _physics_process(delta):
	# follow 
	if target != null and global_position != target.global_position:
		global_position = lerp(global_position,target.global_position,speed*delta)
	
	var mouse_pos = get_viewport().get_mouse_position()
	var dz_upleft = get_viewport_rect().size*(Vector2.ONE-mouse_dead_zone)
	var dz_downright = get_viewport_rect().size*mouse_dead_zone
	if  mouse_pos.x >= dz_downright.x or mouse_pos.x <= dz_upleft.x or mouse_pos.y >= dz_downright.y or mouse_pos.y <= dz_upleft.y:
		var new_position = player.global_position.direction_to(get_global_mouse_position())*mouse_offset
		offset = lerp(offset, new_position, offset_speed*delta)
	else :
		offset = lerp(offset, idle_offset, (offset_speed)*delta)
	
	# shake
	if shake_timer < shake_duration:
		shake_timer += delta
	if shake_timer >= shake_duration:
		_on_shake_timeout()
	
	if shake:
		var damping = ease((shake_duration-shake_timer)/shake_duration,SHAKE_DAMP_EASING)
		offset = Vector2(rand_range(shake_amplitude.x,-shake_amplitude.x),rand_range(shake_amplitude.y,-shake_amplitude.y))
		offset *= damping
		offset += idle_offset

func change_target(node):
	drag_margin_h_enabled = node == player
	drag_margin_v_enabled = node == player
	offset = idle_offset if node == player else Vector2.ZERO
	target = node


func _on_shake_timeout():
	shake = false
	
func _camera_shake(duration : float):
	shake_duration = duration
	shake = true
	shake_timer = 0