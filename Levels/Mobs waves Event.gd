extends Node2D

onready var player = get_node("/root/Level/Player")
onready var camera = get_node("/root/Level/Camera2D")
var started = false
var remaining

func _ready():
	get_node("/root/Level/Ennemies/EnemyEvent1").connect("ennemi_died", self, "enemy_died")
	get_node("/root/Level/Ennemies/EnemyEvent2").connect("ennemi_died", self, "enemy_died")
	get_node("/root/Level/Ennemies/EnemyEvent3").connect("ennemi_died", self, "enemy_died")

func reset_event():
	$EventDoor1.open()
	camera.change_target(player)

func setup_event(area):
	if !started:
		#camera.target = $CameraAnchor
		camera.change_target($CameraAnchor)
		$EventDoor1.close()
		remaining = 3
		started = true

func end_event():
	if started:
		camera.change_target(player)
		$EventDoor1.open()
		$EventDoor2.open()

func enemy_died():
	remaining -= 1
	if remaining <= 0:
			end_event()