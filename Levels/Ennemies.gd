extends Node

onready var level = get_node("/root/Level")
var rock

func _ready():
	rock = level.find_node("Rock")
	for child in get_children():
		child.connection()