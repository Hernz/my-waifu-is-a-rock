extends Node2D

const MAX_MOUSE_SPEED = 20
const JOYSTICK_LIMIT = 0.3

signal reseted()

class_name EmptyLevel 

onready var current_checkpoint : Vector2 = $Player.position
onready var current_reset_area : Area2D = null

func _ready():
	pass

func _on_player_dies():
	var rock = self.find_node("Rock",true,false).get_node("PickUpBox")
	rock.call_deferred("reparent")
	$Player.position = current_checkpoint
	if current_reset_area != null:
		current_reset_area.reset()

func _input(event):
	if event.is_action_pressed("ui_retry"):
		$Player.die()
		emit_signal("reseted")
	
	#Controler mouse wrap
	var X_axis = Input.get_joy_axis(0,JOY_AXIS_2)
	var Y_axis = Input.get_joy_axis(0,JOY_AXIS_3)
	X_axis = 0 if X_axis > -JOYSTICK_LIMIT and X_axis < JOYSTICK_LIMIT else X_axis
	Y_axis = 0 if Y_axis > -JOYSTICK_LIMIT and Y_axis < JOYSTICK_LIMIT else Y_axis
	var joystick_direction : Vector2 = Vector2(X_axis,Y_axis)
	get_viewport().warp_mouse(get_viewport().get_mouse_position() + MAX_MOUSE_SPEED*joystick_direction)

func _on_level_change(level_path):
	if level_path == "fin":
		$Player/EndGameMsg.show()
		get_tree().paused = true
	else :
		$Player.set_physics_process(false)
		$Player.set_process_input(false)
		$Player.velocity.x = $Player.max_speed
		$AnimationPlayer.play("fade")
		yield($AnimationPlayer,"animation_finished")
		get_tree().change_scene(level_path)

func _on_checkpoint_changed(checkpoint,reset_area):
	current_checkpoint = checkpoint
	if current_reset_area != null:
		current_reset_area.delete_trash()
	current_reset_area = reset_area