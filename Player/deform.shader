shader_type canvas_item;

uniform sampler2D deformerTexture;
uniform vec2 tiling = vec2(1);
uniform float intensity : hint_range(0,1);
uniform vec2 displacement = vec2(1);
uniform vec2 speed=vec2(1);


void fragment(){
	vec2 uvDeformer = UV*tiling;
	vec2 deformer = texture(deformerTexture,uvDeformer+TIME*speed).xy-0.5;
	vec2 uvOffset = deformer*intensity*0.5*displacement;
	if(UV.y>0.5)
		uvOffset = vec2(0,0);
	COLOR = texture(TEXTURE,UV+uvOffset);
	if(UV.y+uvOffset.y < 0.1)
		COLOR = COLOR * 1.5;
}