extends Area2D


func _on_enemie_hit(area):
	if self == area:
		get_parent().die()
		
func _on_entering_levelDoor(area):
	if area == self:
		get_parent().can_shoot = false
		
func _on_exiting_levelDoor(area):
	if area == self:
		get_parent().can_shoot = true