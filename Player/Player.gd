extends KinematicBody2D

signal player_dies()

#movement vars
export var acceleration : float = 10
export var deacceleration : float  = 10
export var max_speed : float = 400

#jump var
export var in_air_acceleration : float = 10
export var in_air_deacceleration : float = 10
export var jump_distances : Vector2 = Vector2(200,100)

#dash vars
export var dash_length : int = 12000
export var dash_time : float = 0.2
#export var dash_cooldown : float = 1

export var XYRock : Vector2 = Vector2(600,600)
export var rebondRock : float = 0.3
export var speedRock : float = 200

onready var level = get_node("/root/Level")
onready var rock = level.find_node("Rock")
# screen shake
onready var camera = level.find_node("Camera2D")

var jump : bool = false
var jump_time : float = (jump_distances.x/2)/max_speed
var idle_gravity : float = (2*jump_distances.y)/(pow(jump_time,2))

var dash_direction : Vector2 = Vector2.ZERO
var dash_timer : float = 0
var can_dash : bool = true
#var dash_cooldown_timer : float = 0

#var direction : Vector2 = Vector2()
var velocity : Vector2 = Vector2()
var speed : float = 0
var gravity : float = 0

# state
var jumping : bool = true
var dashing : bool = false
var has_rock : bool = false
var can_shoot : bool = true

func _ready():
	connect("player_dies",get_parent(),"_on_player_dies")
	if self.has_node("Rock"):
		has_rock = true
	gravity = idle_gravity
	rock.speed = speedRock
	rock.rebond = rebondRock

func _process(delta):
	if has_rock:
		rock.previsualiser(position.direction_to(get_global_mouse_position()),XYRock.x,XYRock.y,0.05)
		
		
func _physics_process(delta):
	
	if dashing and dash_timer < dash_time:
		dash_timer += delta
		velocity = dash_direction.normalized()*(dash_length/dash_time)*delta
	else:
		# X axis
		dash_direction = Vector2.ZERO
		if Input.is_action_pressed("ui_right"):
			speed = lerp(speed,max_speed,(in_air_acceleration if jumping else acceleration)*delta)
			$LesSprites.scale.x = abs($LesSprites.scale.x)
			dash_direction += Vector2.RIGHT
		elif Input.is_action_pressed("ui_left"):
			$LesSprites.scale.x = -1*abs($LesSprites.scale.x)
			speed = lerp(speed,-max_speed,(in_air_acceleration if jumping else acceleration)*delta)
			dash_direction += Vector2.LEFT
		else:
			speed = lerp(speed,0,(in_air_deacceleration if jumping else deacceleration)*delta)
		velocity.x = speed
		
		"""
		if Input.is_action_pressed("ui_down"):
			dash_direction += Vector2.DOWN
		elif Input.is_action_pressed("ui_up"):
			dash_direction += Vector2.UP
		"""
		
		# Gravity
		if velocity.x != 0:
			gravity = (2*jump_distances.y*pow(max_speed,2))/(pow(jump_distances.x/2,2))
		else :
			gravity = idle_gravity
		# Y axis
		if is_on_floor() and not jumping :
			velocity.y = 0
		else:
			velocity.y += gravity * delta
	
	
	if(abs(velocity.x) > 50):
		$AnimationPlayer.play("Run")
	else:
		$AnimationPlayer.play("Idle")
		
	if dashing:
		camera._camera_shake(0.1)
		$AnimationPlayer.play("Dashing")
	elif(velocity.y > 100):
		$AnimationPlayer.play("Falling")
		
	#print(velocity)
	move_and_slide(velocity,Vector2.UP)
	
	if dashing and dash_timer >= dash_time:
		velocity = Vector2.ZERO
		dashing = false
	
	if is_on_floor():
		can_dash = true
		jumping = false
	
	if not jumping and jump:
		if velocity.x != 0:
			velocity.y = - (2*jump_distances.y*abs(max_speed))/(jump_distances.x/2)
		else :
			velocity.y = - (2*jump_distances.y)/(jump_time)
		jumping = true
	
	if has_rock:
		if position.direction_to(get_global_mouse_position()).x < 0:
			$Rock.position = Vector2(-40,0)
		else:
			$Rock.position = Vector2(40,0)

func _input(event):
	if event.is_action_pressed("ui_jump"):
		jump = true
		if jumping and not dashing and can_dash and dash_direction != Vector2.ZERO:
			$Sounds/Dash.play()
			dash_timer = 0
			can_dash = false
			dashing = true
	elif event.is_action_released("ui_jump"):
		jump = false
	
	if event.is_action_pressed("ui_shoot") and can_shoot:
		shoot(position.direction_to(get_global_mouse_position()))

func set_has_rock(b :bool):
	if b:
		set_collision_mask_bit(3,false)
		$LesSprites/Rotation_Helper/LesCorps/visage2.visible = true
		$LesSprites/Rotation_Helper/LesCorps/visage.visible = false
	else:
		set_collision_mask_bit(3,true)
		$LesSprites/Rotation_Helper/LesCorps/visage2.visible = false
		$LesSprites/Rotation_Helper/LesCorps/visage.visible = true
	has_rock = b


func shoot(direction):
	if has_rock:
		$Sounds/Throw.play()
		$Rock.shoot(direction,XYRock.x,XYRock.y)
		rock.masquer_previsualisation()
		
func die():
	$AnimationPlayer.play("Die")
	set_physics_process(false)
	rock.visible = false
	yield(get_tree().create_timer(1.0),"timeout")
	set_physics_process(true)
	rock.visible = true
	emit_signal("player_dies")